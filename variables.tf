variable "vm_name" {
  type        = string
  description = "Label da VM. Padrao eh name-terraform"
  default     = "vm-terraform-default"
}

variable "zone" {
  type        = string
  description = "Zona que sera usada"
  default     = "us-central1-c"
}

variable "region" {
  type        = string
  description = "Regiao que sera usada"
  default     = "us-central1"
}

variable "project_id" {
  type        = string
  description = "Projeto que sera usado"
  default     = "august-tangent-397413"
}

variable "machine_type" {
  type        = string
  description = "Tipo de VM"
  default     = "e2-micro"
}