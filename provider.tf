// Configura a versão do Terraform e a versão do provedor Google Cloud
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.73.0"
    }
  }

  //backend "local" {
  backend "http" {
  }

  required_version = ">= 0.15.0"
}

// Configura o provedor Google Cloud com as definições de projeto, região e zona
provider "google" {
  //credentials = file("curso-devops-396702-6995ed377324.json")
  project     = var.project_id // Substitua pelo seu ID de projeto real - https://console.cloud.google.com/
  region      = var.region
  zone        = var.zone
}